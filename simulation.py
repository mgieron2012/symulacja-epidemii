from moving_unit import MovingUnit
from staying_unit import StayingUnit
from random import randint
from itertools import combinations, product
from datetime import datetime, timedelta
from statistic import Statistic
from state import State
from utils import calculate_collision_angle


class Simulation:
    def __init__(self, canvas_width=100, canvas_height=200):
        self._units = []
        self._is_running = False
        self._last_updated = datetime.now()
        self.current_state = State(0, 0, 0, 0)
        self._statistic = Statistic()
        self._canvas_width = canvas_width
        self._canvas_height = canvas_height

    def start(self,
              number_of_moving_units=30,
              number_of_non_moving_units=30,
              infecting_time=100,
              speed=2,
              unit_radius=7
              ):
        max_start_x = self._canvas_width - 2 * unit_radius
        max_start_y = self._canvas_height - 2 * unit_radius

        self.clear()
        self._units = [MovingUnit(randint(0, max_start_x),
                                  randint(0, max_start_y),
                                  unit_radius, infecting_time,
                                  self._canvas_width,
                                  self._canvas_height)
                       for _ in range(number_of_moving_units)]
        self._units += [StayingUnit(randint(0, max_start_x),
                                    randint(0, max_start_y),
                                    unit_radius, infecting_time)
                        for _ in range(number_of_non_moving_units)]
        self._infect_random_unit()
        self._is_running = True
        self._last_updated = datetime.now()
        self._speed = speed
        self._unit_radius = unit_radius
        self._statistic.prepare_for_new_simulation()

    def pause(self):
        if self._is_running:
            self._is_running = False
        elif self._units:
            self._is_running = True
            self._last_updated = datetime.now()

    def clear(self):
        self._is_running = False
        self._units = []
        self.current_state = State(0, 0, 0, 0)

    def _infect_random_unit(self):
        self._units[randint(0, len(self._units)-1)].infect()

    def update(self):
        if not (self._is_running and len(self._units)):
            return
        deltatime = (datetime.now() - self._last_updated) / \
            timedelta(seconds=1) * self._speed
        self._last_updated = datetime.now()
        for unit in self._units:
            unit.update(deltatime)
        self.check_collisions()
        self.update_current_state(deltatime)

    def check_collisions(self):
        for unit1, unit2 in self._pairs_of_possibly_colliding_units():
            if unit1.is_colliding(unit2):
                self.units_collision(unit1, unit2)

    def _pairs_of_possibly_colliding_units(self):
        block_width = self._units[0].r() * 2
        number_of_blocks = int(self._canvas_width / block_width)
        blocks = [[] for _ in range(number_of_blocks)]
        for unit in self._units:
            if 0 <= (x := int(unit.x() // block_width)) < number_of_blocks:
                blocks[x].append(unit)
            else:
                blocks[number_of_blocks - 1].append(unit)
        pairs_to_check = []
        for block in blocks:
            pairs_to_check.extend(combinations(block, 2))

        for i in range(number_of_blocks - 1):
            pairs_to_check.extend(product(blocks[i], blocks[i+1]))
        return pairs_to_check

    def units_collision(self, unit1, unit2):
        collision_angle = calculate_collision_angle(unit1.o(), unit2.o())
        unit1.collision(collision_angle)
        unit2.collision((collision_angle + 180) % 360)

        if unit1.is_infecting():
            unit2.infect()
        elif unit2.is_infecting():
            unit1.infect()

    def is_running(self):
        return self._is_running

    def set_speed(self, speed):
        self._speed = speed

    def number_of_ill_units(self):
        return sum(unit.is_infected() for unit in self._units)

    def number_of_active_units(self):
        return sum(unit.is_infecting() for unit in self._units)

    def number_of_healthy_units(self):
        return sum((not unit.is_infected()) for unit in self._units)

    def generate_graph(self):
        self._statistic.generate_graph()

    def update_current_state(self, deltatime):
        self.current_state.time_since_start += deltatime
        self.current_state.number_of_healthy_units = self.number_of_healthy_units()
        self.current_state.number_of_infected_units = self.number_of_ill_units()
        self.current_state.number_of_infecting_units = self.number_of_active_units()
        self._statistic.add_state(self.current_state)

    def get_render_data(self):
        return map(lambda unit: unit.get_render_data(), self._units)
