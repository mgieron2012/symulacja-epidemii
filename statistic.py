from state import State
import matplotlib.pyplot as plt


class Statistic:
    def __init__(self):
        self._init()
        self._previous = []

    def _init(self):
        self._timestamps = [0]
        self._numbers_of_ill_units = [0]
        self._numbers_of_infecting_units = [0]

    def add_state(self, state: State):
        self._timestamps.append(state.time_since_start)
        self._numbers_of_ill_units.append(state.number_of_infected_units)
        self._numbers_of_infecting_units.append(
            state.number_of_infecting_units)

    def prepare_for_new_simulation(self):
        if len(self._timestamps) == 1:
            return
        self._previous.append(
            [self._timestamps, self._numbers_of_ill_units,
             self._numbers_of_infecting_units])
        self._init()

    def generate_graph(self):
        fig, ax = plt.subplots()
        ax.plot(self._timestamps, self._numbers_of_ill_units,
                label='Liczba zakażonych osób')
        ax.plot(self._timestamps, self._numbers_of_infecting_units,
                label='Liczba zarażających osób')
        for idx, previous in enumerate(self._previous):
            ax.plot(previous[0], previous[1],
                    label=f'Liczba zakażonych osób {idx + 1}')
            ax.plot(previous[0], previous[2],
                    label=f'Liczba zarażających osób {idx + 1}')

        ax.legend(loc='upper left')
        ax.set_title('Symulacja choroby zakaźnej')
        ax.set_xlabel('Czas')
        ax.set_ylabel('Liczba osób')
        plt.show()
