from my_window import MyWindow
from unit import Unit


def canvas():
    return MyWindow().canvas()


def test_init():
    unit = Unit(canvas(), 10, 15, 20, 5)
    assert unit.x() == 10
    assert unit.y() == 15
    assert unit.r() == 20


def test_set_position():
    unit = Unit(canvas(), 10, 15, 20, 5)
    unit.set_position(100, 200)
    assert unit.x() == 100
    assert unit.y() == 200
    assert unit.r() == 20


def test_move():
    unit = Unit(canvas(), 10, 15, 20, 5)
    unit._move(50, 80)
    assert unit.x() == 60
    assert unit.y() == 95
    assert unit.r() == 20
