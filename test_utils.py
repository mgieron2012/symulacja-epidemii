import utils
from pytest import approx


def test_direction_in_degrees_to_vector():
    x, y = utils.direction_in_degrees_to_vector(180)
    assert x == approx(-1)
    assert y == approx(0)


def test_direction_in_degrees_to_vector_2():
    x, y = utils.direction_in_degrees_to_vector(270)
    assert x == approx(0)
    assert y == approx(-1)


def test_points_to_vector():
    x, y = utils.points_to_vector((10, 3), (20, 7))
    assert x == approx(10)
    assert y == approx(4)


def test_vector_to_direction_in_degrees():
    assert utils.vector_to_direction_in_degrees((1, 1)) == approx(45)


def test_vector_to_direction_in_degrees_2():
    assert utils.vector_to_direction_in_degrees((0, -1)) == approx(270)
