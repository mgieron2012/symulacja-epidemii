import math


def direction_in_degrees_to_vector(angle):
    angle_in_radians = math.radians(angle)
    return (math.cos(angle_in_radians), math.sin(angle_in_radians))


def points_to_vector(pointA, pointB):
    return pointB[0] - pointA[0], pointB[1] - pointA[1]


def vector_to_direction_in_degrees(vector):
    x, y = vector
    return math.degrees(math.atan2(y, x)) % 360


def calculate_collision_angle(collision_object_center1,
                              collision_object_center2):
    centers_vector = points_to_vector(
        collision_object_center1, collision_object_center2)
    return vector_to_direction_in_degrees(centers_vector)
