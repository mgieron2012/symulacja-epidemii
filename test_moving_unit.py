from moving_unit import MovingUnit
from my_window import MyWindow

canvas_height = 600
canvas_width = 1000

def test_collision_with_wall_up():
    unit = MovingUnit(100, 0, 5, 10, canvas_width, canvas_height)
    unit.set_direction(90)
    unit.collision(90)
    assert unit._direction == 270


def test_collision_with_wall_up_2():
    unit = MovingUnit(100, 0, 5, 10, canvas_width, canvas_height)
    unit.set_direction(150)
    unit.collision(90)
    assert unit._direction == 210


def test_collision_with_wall_down():
    unit = MovingUnit(100, canvas_height - 10, 5, 10, canvas_width, canvas_height)
    unit.set_direction(270)
    unit.collision(270)
    assert unit._direction == 90


def test_collision_with_wall_down_2():
    unit = MovingUnit(100, canvas_height - 10, 5, 10, canvas_width, canvas_height)
    unit.set_direction(310)
    unit.collision(270)
    assert unit._direction == 50


def test_collision_with_wall_right():
    unit = MovingUnit(canvas_width - 10, 100, 5, 10, canvas_width, canvas_height)
    unit.set_direction(0)
    unit.collision(0)
    assert unit._direction == 180


def test_collision_with_wall_right_2():
    unit = MovingUnit(canvas_width - 10, 100, 5, 10, canvas_width, canvas_height)
    unit.set_direction(30)
    unit.collision(0)
    assert unit._direction == 150


def test_collision_with_wall_left():
    unit = MovingUnit(0, 100, 5, 10, canvas_width, canvas_height)
    unit.set_direction(180)
    unit.collision(180)
    assert unit._direction == 0


def test_collision_with_wall_left_2():
    unit = MovingUnit(0, 100, 5, 10, canvas_width, canvas_height)
    unit.set_direction(100)
    unit.collision(180)
    assert unit._direction == 80


def test_collision_with_wall_left_3():
    unit = MovingUnit(0, 100, 5, 10, canvas_width, canvas_height)
    unit.set_direction(200)
    unit.collision(180)
    assert unit._direction == 340


def test_collision():
    unit = MovingUnit(100, 100, 5, 10, canvas_width, canvas_height)
    unit.set_direction(45)
    unit.collision(45)
    assert unit._direction == 225


def test_collision_almost_same_direction():
    unit = MovingUnit(100, 100, 5, 10, canvas_width, canvas_height)
    unit.set_direction(45)
    unit.collision(180)
    assert unit._direction == 45
