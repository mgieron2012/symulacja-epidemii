from my_window import MyWindow
from simulation import Simulation
from datetime import datetime, timedelta


class Program:
    def __init__(self):
        self.window = MyWindow()
        canvas_width = self.window.canvas().width()
        canvas_height = self.window.canvas().height()
        self.simulation = Simulation(canvas_width, canvas_height)
        self._last_frame = datetime.now()

    def run(self):
        while True:
            self.window.read(timeout=1)
            if self.window.is_close_window_triggered():
                self.simulation.clear()
                break

            if self.window.event == "_Start_":
                self.simulation.clear()
                self.clear_canvas()
                try:
                    self.simulation.start(*self.window.get_input())
                except ValueError:
                    self.show_incorrect_input_popup()

            if self.window.event == "_Pause_":
                self.simulation.pause()

            if self.window.event == "_Reset_":
                self.simulation.clear()
                self.clear_canvas()

            if self.window.event == "_Plot_":
                self.simulation.pause()
                self.generate_graph()

            self.update()

        self.clear_canvas()
        self.window.close()

    def update(self):
        self.simulation.update()
        self.refresh_stats()
        data_to_render = self.simulation.get_render_data()
        self.window.canvas().render(data_to_render)
        self.refresh_fps()
        self.update_simulation_speed(self.window.get_input("_SPEED_"))

    def refresh_fps(self):
        now = datetime.now()
        fps = round(timedelta(seconds=1) / (now - self._last_frame))
        self._show_fps(fps)
        self._last_frame = now

    def _show_fps(self, fps):
        self.window.show_fps(fps)

    def clear_canvas(self):
        self.window.canvas().clear()

    def refresh_stats(self):
        self.window.show_current_state(self.simulation.current_state)

    def generate_graph(self):
        self.simulation.generate_graph()

    def update_simulation_speed(self, speed):
        self.simulation.set_speed(speed)

    def show_incorrect_input_popup(self):
        self.window.show_incorrect_input_popup()


if __name__ == "__main__":
    Program().run()
