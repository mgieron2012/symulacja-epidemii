from infecting_sign import InfectingSign
from math import sqrt


class Unit:
    def __init__(self, x, y, r, infecting_time):
        self._is_infected = False
        self._is_infecting = False
        self._infecting_time = infecting_time
        self._x = x
        self._y = y
        self._r = r

    def is_infecting(self):
        return self._is_infecting

    def is_infected(self):
        return self._is_infected

    def set_position(self, x, y):
        self._x = x
        self._y = y

    def _move(self, x, y):
        self._x += x
        self._y += y

    def x(self):
        return self._x

    def y(self):
        return self._y

    def r(self):
        return self._r

    def o(self):
        return self._x + self._r, self._y + self._r

    def direction(self):
        if self.is_moving():
            return self._direction

    def is_moving(self):
        return hasattr(self, '_direction')

    def infect(self):
        if self.is_infected():
            return
        self._is_infected = self._is_infecting = True
        self._infecting_sign = InfectingSign(self._infecting_time)

    def infecting_state_update(self, deltatime):
        if not self._is_infecting:
            return
        self._infecting_sign.update(deltatime)
        if self._infecting_sign.infecting_times_up():
            del self._infecting_sign
            self._is_infecting = False

    def update(self, deltatime):
        raise NotImplementedError

    def collision(self, *args):
        raise NotImplementedError

    def distance_to_other_unit(self, other_unit):
        return sqrt((self.x() - other_unit.x()) ** 2
                    + (self.y() - other_unit.y()) ** 2)

    def is_colliding(self, other_unit):
        distance = self.distance_to_other_unit(other_unit)
        return distance <= self.r() + other_unit.r()

    def get_render_data(self):
        return {
            "x": self.x(),
            "y": self.y(),
            "r": self.r(),
            "color": "yellow" if self.is_infected() else "green",
            "arc_angle": self._infecting_sign.arc_angle()
            if self.is_infecting() else 0
        }
