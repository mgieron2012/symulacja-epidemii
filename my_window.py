import PySimpleGUI as sg
from layout import Layout
from canvas import Canvas


class MyWindow:
    def __init__(self):
        self._set_theme()
        self._window = sg.Window(
            'Symulacja rozwoju choroby zakaźnej',
            layout=Layout.create(), finalize=True)
        self.event = None
        self._canvas = Canvas(self._window['canvas'].TKCanvas)

    def _set_theme(self, theme="DarkAmber"):
        sg.theme(theme)

    def canvas(self):
        return self._canvas

    def read(self, timeout=1):
        self.event, self.input_values = self._window.read(timeout=timeout)

    def is_close_window_triggered(self):
        return self.event == sg.WIN_CLOSED or self.event == 'Exit'

    def get_input(self, key=""):
        if key == "":
            return (int(self.input_values.get("_START_UNITS_MOVING_")),
                    int(self.input_values.get("_START_UNITS_STAYING_")),
                    float(self.input_values.get("_INFECTING_TIME_")),
                    int(self.input_values.get("_SPEED_")))
        else:
            return int(self.input_values.get(key))

    def close(self):
        return self._window.close()

    def show_current_state(self, state):
        self._window['_ACTUAL_HEALTHY_'].Update(
            f'Aktualna liczba zdrowych jednostek: '
            f'{state.number_of_healthy_units}')
        self._window['_ACTUAL_ILL_'].Update(
            f'Aktualna liczba chorych jednostek: '
            f'{state.number_of_infected_units}')
        self._window['_ACTUAL_ILL_INACTIVE_'].Update(
            f'W tym liczba zarażających jednostek: '
            f'{state.number_of_infecting_units}')
        self._window['_ACTUAL_TIME_'].Update(
            f'Czas od rozpoczęcia: {int(state.time_since_start)}')

    def show_fps(self, fps):
        self.canvas().show_fps(fps)

    def show_incorrect_input_popup(self):
        sg.popup("Niepoprawne parametry symulacji",
                 no_titlebar=True)
