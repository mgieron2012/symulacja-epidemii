class InfectingSign:
    def __init__(self, infecting_time_in_seconds):
        self._infecting_time_in_ms = infecting_time_in_seconds * 1000
        self._time_since_infected = 0
        self._arc_angle = 359.99

    def update(self, deltatime):
        self._time_since_infected += deltatime
        self._arc_angle = (1 - (self._time_since_infected /
                                self._infecting_time_in_ms)) * 360

    def arc_angle(self):
        return self._arc_angle

    def infecting_times_up(self):
        return self.arc_angle() < 0
