from dataclasses import dataclass


@dataclass
class State:
    time_since_start: float
    number_of_healthy_units: int
    number_of_infected_units: int
    number_of_infecting_units: int
