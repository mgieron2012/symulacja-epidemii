# Dokumentacja projektu

## Cel projektu
Wykonanie aplikacji desktopowej symulującej rozprzestrzenianie się choroby zakaźnej na przykładzie poruszających się kul w ograniczonej przestrzeni dwuwymiarowej. 

## Specyfikacja wymagań
Aplikacja wizualizuje symulację rozprzestrzeniania się choroby zakaźnej na przykładzie poruszających się kul w ograniczonej przestrzeni dwuwymiarowej. Na początku tylko jedna z kul jest nosicielem choroby. Kule poruszają się zgodnie z losowym wektorem kierunku oraz stałą prędkością początkową. W momencie gdy kula zarażona zderzy się z kulą niezarażoną, następuje transmisja choroby. Zderzenia kul oraz kuli ze ścianą ograniczającą przestrzeń są elastyczne, obowiązują prawa fizyki. Każda z zarażonych kul może zarażać tylko przez określony czas, po czym przechodzi w stan, w którym jest chora, ale nie zaraża innych. Ponadto, część kul jest nieruchoma. 

Parametrami wejściowymi programu są:
- początkowa liczba kul poruszających się
- początkowa liczba kul nieporuszających się
- czas przez który zarażona kula jest w stanie zarażać inne
- szybkość symulacji.

Aplikacja podczas trwania symulacji pokazuje aktualną liczbę kul zdrowych, chorych, zarażających i czas od jej rozpoczęcia. Oprócz tego daje możliwość wygenerowania wykresu pokazującego przebieg symulacji. 

## Dokumentacja użytkownika
Program uruchamia się poprzez uruchomienie pliku main.py poprzez interpreter Pythona.
Po uruchomieniu aplikacji widoczne jest szare okno reprezentujące przestrzeń, w której przeprowadzona zostanie symulacja. Po prawej stronie znajdują się przyciski służące do:
- rozpoczęcia nowej symulacji (kliknięcie w trakcie trwającej symulacji kończy trwającą symulację i rozpoczyna następną)
- zatrzymania/wznowienia symulacji
- wygenerowania wykresu (zatrzymuje bieżącą symulację i generuje wykres trwającej symulacji oraz wszystkich zakończonych)
- zresetowania symulacji.

Oprócz przycisków widoczne jest okno *statystyki symulacji* z aktualnymi statystykami przeprowadzanej symulacji. W oknie *parametry symulacji* ustawić można parametry wejściowe programu: 
- początkową liczbę kul poruszających się
- początkowa liczba kul nieporuszających się
- czas przez który zarażona kula jest w stanie zarażać inne
> Powyższe parametry odczytywane są jedynie przy rozpoczęciu symulacji. Ich zmiana będzie skutkowała na następną rozpoczętą symulację.
- szybkość symulacji
> Parametr *szybkość symulacji* odnosi się jedynie do wizualizacji symulacji i nie ma wpływu na jej wynik.

Podczas symulacji zdrowe jednostki reprezentowane są przez zielone kule, chore jednostki przez żółte kule. Zdolność do zarażania przez chore jednostki sygnalizowana jest poprzez czerwony wycinek koła znajdujący się na chorej jednostce. Procent pokrycia kuli przez czerwony jest tożsama z procentem upłyniętego czasu, przez który chora jednostka może zarażać.

## Biblioteki i struktura kodu
Aplikacja napisana jest w języku Python. Przy realizacji projektu wykorzystane zostały biblioteki [PySimpleGui](https://pysimplegui.readthedocs.io/en/latest/) oraz [Matplotlib](https://matplotlib.org/). 

Lista klas:
- Program - tworzy instancje klas MyWindow, Simulation, kontroluje przepływ danych pomiędzy GUI a symulacją
- MyWindow - tworzy okno aplikacji, odczytuje dane wejściowe i zmienia parametry wyświetlane w GUI
- Layout - tworzy layout okna aplikacji
- Canvas - wyświetla grafikę w oknie
- Simulation - odpowiada za przeprowadzenie symulacji
- Statistic - zbiera statystyki symulacji i umożliwia wygenerowanie wykresu
- State - struktura danych reprezentująca stan symulacji
- Unit - reprezentuje jednostkę w symulacji, zawiera pozycję jednostki, jej stan zakażenia
- StayingUnit <- Unit - reprezentuje stojącą jednostkę
- MovingUnit <- Unit - reprezentuje poruszającą się jednostkę, przechowuje kierunek poruszania się i wylicza nowy kierunek po zderzeniu z inną jednostką lub ścianą
- InfectingState - kontroluje czas ubiegający od zainfekowania jednostki i wyświetla znacznik zarażania

## Podsumowanie
Projekt został wykonany zgodnie z zaleceniami, wyświetlany jest obraz symulacji razem z aktualnymi statystykami. Program daje możliwość wygenerowania wykresu i porównania ze sobą przeprowadzonych symulacji. Użytkownik może symulację zapauzować lub przyspieszyć. 
Trudność sprawiło wykorzystanie pełni możliwości nieznanej biblioteki do tworzenia GUI oraz napisanie testów do kodu wykorzystującego nieznane biblioteki.
