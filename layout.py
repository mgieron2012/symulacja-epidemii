import PySimpleGUI as sg


class Layout:
    @staticmethod
    def _configuration_group():
        return [[sg.Text('Początkowa liczba nieporuszających się jednostek: '),
                 sg.Input(key="_START_UNITS_STAYING_",
                          size=(3, 1), default_text="10")],
                [sg.Text('Początkowa liczba poruszających się jednostek: '),
                 sg.Input(key="_START_UNITS_MOVING_",
                          size=(3, 1), default_text="100")],
                [sg.Text('Czas zarażania: '),
                 sg.Input(key="_INFECTING_TIME_",
                          size=(3, 1), default_text="3")],
                [sg.Text('Szybkość przeprowadzenia symulacji: '),
                 sg.Slider(range=(20, 200), key="_SPEED_", default_value=50,
                           orientation="horizontal",
                           disable_number_display=True)]]

    @staticmethod
    def _stats_group():
        return [[sg.Text('Aktualna liczba zdrowych jednostek: 000',
                         key="_ACTUAL_HEALTHY_")],
                [sg.Text('Aktualna liczba chorych jednostek: 000',
                         key="_ACTUAL_ILL_")],
                [sg.Text('W tym liczba zarażających jednostek: 000',
                         key="_ACTUAL_ILL_INACTIVE_")],
                [sg.Text('Czas od rozpoczęcia: 0000000',
                         key="_ACTUAL_TIME_")]
                ]

    @staticmethod
    def _button_group():
        return ([sg.Button('Rozpocznij symulację', key="_Start_")],
                [sg.Button('Zatrzymaj/Wznów symulację', key="_Pause_")],
                [sg.Button('Generuj wykres', key="_Plot_")],
                [sg.Button('Zresetuj symulację', key="_Reset_")])

    @staticmethod
    def create():
        return [[sg.Canvas(size=[1000, 600],
                           background_color="gray", key="canvas"),
                 sg.Frame(title="",
                          layout=[[sg.Frame('Parametry symulacji',
                                            Layout._configuration_group())],
                                  [sg.Frame('Statystyki symulacji',
                                            Layout._stats_group())],
                                  *Layout._button_group()], border_width="0")]]
