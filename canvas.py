class Canvas:
    def __init__(self, tkcanvas):
        self.__canvas = tkcanvas
        self._FPS = self.create_fps_text()

    def width(self):
        return self.__canvas.winfo_width()

    def height(self):
        return self.__canvas.winfo_height()

    def clear(self):
        self.__canvas.delete("all")
        self._FPS = self.create_fps_text()

    def create_fps_text(self):
        return self.__canvas.create_text(30, 10, fill="darkblue", text="0 FPS")

    def show_fps(self, fps):
        self.__canvas.itemconfigure(self._FPS, text=f'{fps} FPS')

    def create_oval(self, x, y, radius=7, color="green"):
        self.__canvas.create_oval(x, y, x+2*radius, y+2*radius, fill=color)

    def create_arc(self, x, y, radius=7, color="red", arc=359.99):
        return self.__canvas.create_arc(x, y, x+2*radius, y+2*radius, start=0,
                                        extent=arc, fill=color, outline=color)

    def render(self, render_data):
        self.clear()
        for unit in render_data:
            self.create_oval(unit['x'], unit['y'],
                             unit['r'], color=unit['color'])
            if unit['arc_angle'] != 0:
                self.create_arc(unit['x'], unit['y'], arc=unit['arc_angle'])
