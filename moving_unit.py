from unit import Unit
from random import randint
from utils import direction_in_degrees_to_vector, calculate_collision_angle


class MovingUnit(Unit):
    def __init__(self, x, y, r, infecting_time, canvas_width, canvas_height):
        super().__init__(x, y, r, infecting_time)
        self.set_direction(randint(0, 359))
        self._canvas_width = canvas_width
        self._canvas_height = canvas_height

    def set_direction(self, direction):
        self._direction = direction % 360
        self._direction_vector = direction_in_degrees_to_vector(
            self._direction)

    def update(self, deltatime):
        self._stay_inside_canvas()
        self.move(deltatime)
        self.infecting_state_update(deltatime)

    def move(self, distance):
        x, y = self._direction_vector
        self._move(x * distance, y * distance)

    def _collision(self, collision_object_center):
        collision_angle = calculate_collision_angle(
            self.o(), collision_object_center)
        self.collision(collision_angle)

    def collision(self, collision_angle=0):
        if not 100 < abs(collision_angle - self._direction) < 260:
            new_direction = 180 - self._direction + 2*collision_angle
            self.set_direction(new_direction)

    def _stay_inside_canvas(self):
        if self.x() <= 0 and 90 < self._direction < 270:
            self.collision(180)
        elif self.x() + self.r() * 2 >= self._canvas_width and \
                not 90 < self._direction < 270:
            self.collision(0)
        if self.y() <= 0 and self._direction > 180:
            self.collision(270)
        elif self.y() + self.r() * 2 >= self._canvas_height and \
                self._direction < 180:
            self.collision(90)
